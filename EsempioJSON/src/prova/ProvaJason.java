package prova;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

public class ProvaJason {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{
		JSONObject scatola = new JSONObject();
		Map<String,String> f = new HashMap<String,String>();
		f.put("Nome","Giuseppe");
		f.put("Indirizzo","Via dei Tigli");		
		List<Integer> l = new LinkedList<Integer>();
		l.add(201);
		l.add(221);
		l.add(341);
		scatola.put("contenuto", "viti");
		scatola.put("quantita'", 100);
		scatola.put("fornitore", f);
		scatola.put("magazzino", l);
		System.out.println(scatola);
		
		String n = (String) scatola.get("contenuto");
		System.out.println(n);
		int x = (Integer) scatola.get("quantità");
		System.out.println(2*x);

	}
}